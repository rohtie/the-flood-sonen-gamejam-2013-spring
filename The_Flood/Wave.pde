class Wave {
  Animation wave = new Animation("water",4,true);
  Animation death = new Animation("water_death",8,false);
  float x;
  
  boolean dead = false;
  
  Wave(float x){
    this.x = x;
  }
  
  void draw(int bgx){
    if(x > 0) {
      if(bgx == 0)
        x-= 2;
      else
        x-= 0.10;
    }
    
    if(!dead)
      wave.draw(x,0,false);
  }
  
  boolean die(){
    dead = true;
    return death.draw(x,0,false);
  }
}
