class Attack {
  Animation fly = new Animation("attack",3,true);
  Animation hit = new Animation("attack_death",5,false);
  
  float x;
  float y;
  boolean right;
  boolean flying = true;
  
  Attack(float x,float y,boolean right){
    if(right)
      this.x = x + 8;
    else
      this.x = x - 8;
    this.y = y + 16;
    this.right = !right;
  }
  
  void checkCollision(Tile tile){
    if(tile.x < x + fly.images[0].width && tile.x+tile.img.width > x){
      if(tile.y < y + fly.images[0].width && tile.y + tile.img.height > y){
          flying = false;

        }
    }
  }
  
  void draw(){
    if(flying){
      fly.draw(x,y,right);
      
      if(right)
        x-=1;
      else
        x+=2;
        
    }else{
      hit.draw(x,y,right);
      x+=1;
    }
        
  }
}
