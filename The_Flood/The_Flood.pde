import ddf.minim.*;
import java.util.*;

Player player;
Tile tile;
ArrayList<Tile> tiles = new ArrayList<Tile>();
PImage bg, end0, end1, cutscene;
int l = -1664 + 256;
int bgx = l;
Wave w;

int cs = 0;

boolean ended = false;
boolean ended2 = false;
boolean started = false;

AudioPlayer aplayer;
AudioPlayer bplayer;
AudioPlayer shoot;
Minim minim;

void setup(){
  
  size(256,256);
  smooth();
  
  w = new Wave(240);
  
  player = new Player(128,128 + 64);
  
  String level = new String(
  "---X-----------------------------------------------------------------------------------------------------\n" +
  "---W-----------------------------------------------------------------------------------------------------\n" +
  "---W-----------------------------------------------------------------------------------------------------\n" +
  "---W-----------------------------------------------------------------------------------------------------\n" +
  "---W-----------------------------------------------------------------------------------------------------\n" +
  "---W--------------------------------XXXX---XXXXX---------------------------------------------------------\n" +
  "---W-------------------------------X----XXX-----X--------------------------------------------------------\n" +
  "---W-----------------------------XX--------------XXX-----------------------------------------------------\n" +
  "---W------------------------X----W------------------X--------------------------XXX-----------------------\n" +
  "---W----------------------XXXX---W-------------------X-------------------------W-W-----------------------\n" +
  "---W----------------XXX----------W--------------------XX---XXXXXX--------XXXXXX---XX--XX-----------------\n" +
  "---W----------------W-W----------W---------------------W---W----W-----XXX----------W--W-X----------------\n" +
  "----XXXXX-----XXXXXX---X-------XX--------------------WWW--X------X--XX-------------W--W--X---------------\n" +
  "---------XXXXX---------W-------W--------------------W-----W------W--W--------------W--W---XXXXXXXXXXXXXXX\n" +
  "-----------------------W-------W-------------------W------W------W--W--------------W--W------------------\n" +
  "-----------------------W-------W--------------------XXXX--W------W--W--------------W--W------------------\n");
  
  Scanner s = new Scanner(level);
  
  int lx = l;
  int ly = 0;
  while(s.hasNextLine()){
    for(char c : s.nextLine().toCharArray()){
      if(c == 'X')
        tiles.add(new Tile(lx,ly,true));
        
      if(c == 'W')
        tiles.add(new Tile(lx,ly,false));
        
      lx += 16;
    }
    ly += 16;
    lx = l;
  }
  
  bg = loadImage("background.png");
  end0 = loadImage("ending0.png");
  end1 = loadImage("ending1.png");
  cutscene = loadImage("cutscene.png");

  minim = new Minim(this);
  aplayer = minim.loadFile("waves.wav");
  bplayer = minim.loadFile("ending.wav");
  shoot = minim.loadFile("eg.wav");
  aplayer.play();
  aplayer.loop();
}

void draw(){
  background(0,25,255);
  
  if(started){  
    player.move(bgx);
    for(Tile t : tiles){
      player.checkCollision(t);
      t.move(bgx);
    }
    
    player.draw();
    
    image(bg,bgx,0);
    
    if(bgx < 0)
      bgx++;
    
    w.draw(bgx);
    
    if(bgx == 0 && player.attacking && player.faceright){
      ended2 = true;
      bgx++;
    } else if(bgx == 0 || round(w.x) == round(player.location.x) - 64){
      ended = true;
      aplayer.pause();
      bplayer.play();
      bplayer.loop();
      bgx++;
    }
  }else{
    switch(cs){
      case 0:
        image(cutscene,-256,0);
        break;
        
      case 1:
        image(cutscene,0,0);
        break;
      
      case 2:
        started = true;
        break;
    }    
  }
  
  if(ended)
    image(end0,0,0);
  
  if(ended2){
    if(!w.die()){
      aplayer.pause();
      bplayer.play();
      bplayer.loop();
      image(end1,0,0);
    }    
  }
  
}

void keyPressed(){
  if(started){
  switch(keyCode){
    case UP:
      player.jump();
      break;
    case LEFT:
      player.left();
      break;
    case RIGHT:
      player.right();
      break;
    case DOWN:
      player.attack();
      break;
  }
}else{
    if(keyCode == DOWN)
      cs++;
  }
     
}

void keyReleased(){ 
  if(started){
    switch(keyCode){
      case LEFT:
        player.notleft();
        break;
      case RIGHT:
        player.notright();
        break;
    }
  }
}