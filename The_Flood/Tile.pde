class Tile{
  PImage img;
  int x,y;
  boolean ontile = false;
  boolean top;
  
  Tile(int x, int y, boolean top){
    this.x = x;
    this.y = y;
    this.top = top;
    img = loadImage("tile2.png");
  }
  
  void move(int bgx){
    if(bgx < 0)
      x+=1;
  }
}
