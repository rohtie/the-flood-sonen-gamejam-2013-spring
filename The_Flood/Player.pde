class Player {
  PVector location;
  PVector velocity;
  PVector gravity;
  
  boolean isjumping = false;
  
  boolean movingleft = false;
  boolean movingright = false;
  
  boolean attacking = false;
  
  boolean ontile = false;
  
  boolean faceright = false;
  
  ArrayList<Attack> attacks = new ArrayList<Attack>();
  
  PImage character;
  Animation walk = new Animation("moses",4,true);
  Animation attack = new Animation("moses_attack",11,false);
  Animation jump = new Animation("moses_jump",7,false);
  
  Player(int x, int y){
    location = new PVector(x,y);
    velocity = new PVector(0,0);
    gravity = new PVector(0,0.02);
    character = loadImage("moses.gif");
  }
  
  void move(int bgx){
    location.add(velocity);
    velocity.add(gravity);
    
    if(isjumping)
      velocity.y += 0.2;
    
    if(movingleft && velocity.x > -2.5)
      velocity.x -= 0.1;
      
    if(movingright && velocity.x < 2.5)
      velocity.x += 0.1;
      
    if(!movingleft && !movingright){
      if(velocity.x > -0.2 && velocity.x < 0.02)
        velocity.x = 0;
      else if(velocity.x > 0)
        velocity.x -= 0.05;
      else
        velocity.x += 0.05;
    }
    
    if(bgx < 0)
      location.x += 1;
  }
  
  void draw(){
     if(isjumping){
       if(!jump.draw(location.x,location.y,faceright)){
         if(faceright){
           pushMatrix();
           scale(-1.0, 1.0);
           image(character,-location.x - character.width,location.y);
           popMatrix();
         }else        
           image(character,location.x,location.y);
       }
         
     }else if(movingleft){
       walk.draw(location.x,location.y,faceright);
       
     }else if(movingright){
       walk.draw(location.x,location.y,faceright);
       
     }else if(attacking){
       if(attack.frame == 5){
         shoot.rewind();
         shoot.play();
       }
       if(attack.frame == 10){
         attacks.add(new Attack(location.x,location.y,faceright));
       }
       
       if(!attack.draw(location.x,location.y,faceright)){
         attacking = false;
         attack.reset();
         
         if(faceright){
           pushMatrix();
           scale(-1.0, 1.0);
           image(character,-location.x - character.width,location.y);
           popMatrix();
         }else        
           image(character,location.x,location.y);
       }
     }
       
     if(!movingleft && !movingright && !isjumping && !attacking)
         if(faceright){
           pushMatrix();
           scale(-1.0, 1.0);
           image(character,-location.x - character.width,location.y);
           popMatrix();
         }else        
           image(character,location.x,location.y);
      
      for(Attack a : attacks)
        a.draw();
  }
  
  void jump(){
      if(!isjumping){
        velocity.y -= 7;
        isjumping = true;
      }
  }
  
  void left(){
    movingleft = true;
    faceright = false;
    attacking = false;
    attack.reset();
  }
  
  void notleft(){
    movingleft = false;
  }
  
  void right(){
    movingright = true;
    faceright = true;
    attacking = false;
    attack.reset();
  }
  
  void notright(){
    movingright = false;
  }
  
  void attack(){
    if(!attacking){
      attacking = true;
    }
  }
    
  void checkCollision(Tile tile){
    for(Attack a : attacks)
      a.checkCollision(tile);
    
    if(tile.x < location.x + character.width && tile.x+tile.img.width > location.x){
      if(tile.y < location.y + character.height && tile.y + tile.img.height > location.y){
        if(tile.top){
          if(velocity.y > 0){
            location.y = tile.y - character.height;
            tile.ontile = true;
            ontile = true;
            isjumping = false;
            velocity.y = 0;
            jump.reset();
          }
        }
        
        if(tile.ontile || ontile)
          return;
        
        if(velocity.x > 0){
          location.x = tile.x - tile.img.width + 1;
          velocity.x -= 0.2;
          
        }else if(velocity.x < 0){
          location.x = tile.x + tile.img.width + 1;
          velocity.x += 0.2;
        }
      }
    }
    
    if(tile.ontile || ontile){
      velocity.y += 1;
      tile.ontile = false;
      ontile = false;
    }
  }
}


