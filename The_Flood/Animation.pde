class Animation {
  PImage[] images;
  int imageCount;
  int frame = 0;
  int rate = 0;
  
  boolean loop;
  
  Animation(String imagePrefix, int count, boolean loop) {
    this.loop = loop;
    imageCount = count;
    images = new PImage[imageCount];

    for (int i = 0; i < imageCount; i++) {
      String filename = imagePrefix + nf(i, 2) + ".png";
      images[i] = loadImage(filename);
    }
  }

  boolean draw(float xpos, float ypos,boolean flip) {
    if(!loop && frame == imageCount - 1)
      return false;
      
    if(rate % 5 == 0)
      frame = (frame+1) % imageCount;
      
    if(flip){
       pushMatrix();
       scale(-1.0, 1.0);
       image(images[frame],-xpos - images[frame].width,ypos);
       popMatrix();
    }else
    image(images[frame], xpos, ypos);
    rate++;
    
    return true;
  }
  
  void reset(){
    frame = 0;
  }
}
